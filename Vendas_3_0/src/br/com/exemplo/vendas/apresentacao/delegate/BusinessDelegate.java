package br.com.exemplo.vendas.apresentacao.delegate;

import java.math.BigDecimal;
import java.rmi.RemoteException;

import br.com.exemplo.vendas.negocio.interfaces.ICliente;
import br.com.exemplo.vendas.negocio.interfaces.ICompra;
import br.com.exemplo.vendas.negocio.interfaces.IPedido;
import br.com.exemplo.vendas.negocio.interfaces.IProduto;
import br.com.exemplo.vendas.negocio.interfaces.RecebeRequisicaoInterface;
import br.com.exemplo.vendas.negocio.interfaces.UsuarioInterface;
import br.com.exemplo.vendas.util.dto.ServiceDTO;
import br.com.exemplo.vendas.util.exception.LayerException;
import br.com.exemplo.vendas.util.exception.SysExceptionFactory;
import br.com.exemplo.vendas.util.locator.ServiceLocator;
import br.com.exemplo.vendas.util.locator.ServiceLocatorException;
import br.com.exemplo.vendas.util.locator.ServiceLocatorFactory;

public class BusinessDelegate {
	private static BusinessDelegate instance = null;

	private ServiceLocator serviceLocator;

	private BusinessDelegate() throws Exception {
		setServiceLocator();
	}

	public synchronized static BusinessDelegate getInstance()
			throws LayerException {
		if (instance == null) {
			try {
				instance = new BusinessDelegate();
			} catch (Exception exception) {
				throw SysExceptionFactory.getException(exception);
			}
		}
		return instance;
	}

	public ServiceDTO inserirUsuario(ServiceDTO requestDTO)
			throws LayerException {
		ServiceDTO responseDTO = new ServiceDTO();
		try {
			responseDTO = ((UsuarioInterface) serviceLocator
					.getService("UsuarioBean/remote"))
					.inserirUsuario(requestDTO);
		} catch (RemoteException remoteException) {
			throw SysExceptionFactory.getException(remoteException);
		} catch (ServiceLocatorException serviceLocatorException) {
			throw SysExceptionFactory.getException(serviceLocatorException);
		}
		return responseDTO;
	}

	public ServiceDTO excluirUsuario(ServiceDTO requestDTO)
			throws LayerException {
		ServiceDTO responseDTO = new ServiceDTO();
		try {
			responseDTO = ((UsuarioInterface) serviceLocator
					.getService("UsuarioBean/remote"))
					.excluirUsuario(requestDTO);
		} catch (RemoteException remoteException) {
			throw SysExceptionFactory.getException(remoteException);
		} catch (ServiceLocatorException serviceLocatorException) {
			throw SysExceptionFactory.getException(serviceLocatorException);
		}
		return responseDTO;
	}

	public ServiceDTO alterarUsuario(ServiceDTO requestDTO)
			throws LayerException {
		ServiceDTO responseDTO = new ServiceDTO();
		try {
			responseDTO = ((UsuarioInterface) serviceLocator
					.getService("UsuarioBean/remote"))
					.alterarUsuario(requestDTO);
		} catch (RemoteException remoteException) {
			throw SysExceptionFactory.getException(remoteException);
		} catch (ServiceLocatorException serviceLocatorException) {
			throw SysExceptionFactory.getException(serviceLocatorException);
		}
		return responseDTO;
	}

	public ServiceDTO selectionarTodosUsuarios(ServiceDTO requestDTO)
			throws LayerException {
		ServiceDTO responseDTO = new ServiceDTO();
		try {
			responseDTO = ((UsuarioInterface) serviceLocator
					.getService("UsuarioBean/remote"))
					.selecionarTodosUsuario(requestDTO);
		} catch (RemoteException remoteException) {
			throw SysExceptionFactory.getException(remoteException);
		} catch (ServiceLocatorException serviceLocatorException) {
			throw SysExceptionFactory.getException(serviceLocatorException);
		}
		return responseDTO;
	}

	public ServiceDTO inserirQueue(ServiceDTO requestDTO) throws LayerException {
		ServiceDTO responseDTO = new ServiceDTO();
		try {
			responseDTO = ((RecebeRequisicaoInterface) serviceLocator
					.getService("RecebeRequisicaoBean/remote"))
					.inserirFila(requestDTO);
		} catch (RemoteException remoteException) {
			throw SysExceptionFactory.getException(remoteException);
		} catch (ServiceLocatorException serviceLocatorException) {
			throw SysExceptionFactory.getException(serviceLocatorException);
		}
		return responseDTO;
	}

	private void setServiceLocator() throws Exception {
		this.serviceLocator = ServiceLocatorFactory
				.getServiceLocator("serviceLocator");

	}

	public ServiceDTO getClientesQuePossuemCompras() throws LayerException {
		ServiceDTO responseDTO = new ServiceDTO();
		try {
			responseDTO = ((ICliente) serviceLocator
					.getService("ClienteBean/remote"))
					.consulta(null);
		} catch (RemoteException remoteException) {
			throw SysExceptionFactory.getException(remoteException);
		} catch (ServiceLocatorException serviceLocatorException) {
			throw SysExceptionFactory.getException(serviceLocatorException);
		}
		return responseDTO;
	}

	public ServiceDTO getComprasMenoresQueQuinhentos() throws LayerException {
		ServiceDTO responseDTO = new ServiceDTO();
		try {
			
			ServiceDTO requestDTO = new ServiceDTO();
			requestDTO.set ("valorInicial", new BigDecimal(0));
			requestDTO.set("valorFinal", new BigDecimal(500));
			
			responseDTO = ((ICompra) serviceLocator
					.getService("CompraBean/remote"))
					.consulta(requestDTO);
			
		} catch (RemoteException remoteException) {
			throw SysExceptionFactory.getException(remoteException);
		} catch (ServiceLocatorException serviceLocatorException) {
			throw SysExceptionFactory.getException(serviceLocatorException);
		}
		return responseDTO;
	}

	public ServiceDTO getPedidosComReserva() throws LayerException {
		ServiceDTO responseDTO = new ServiceDTO();
		try {
			responseDTO = ((IPedido) serviceLocator
					.getService("PedidoBean/remote"))
					.consulta(null);
		} catch (RemoteException remoteException) {
			throw SysExceptionFactory.getException(remoteException);
		} catch (ServiceLocatorException serviceLocatorException) {
			throw SysExceptionFactory.getException(serviceLocatorException);
		}
		return responseDTO;
	}

	public ServiceDTO getProdutosQueCustamMenosQueMil() throws LayerException {
		ServiceDTO responseDTO = new ServiceDTO();
		try {
			responseDTO = ((IProduto) serviceLocator
					.getService("ProdutoBean/remote"))
					.consulta(null);
		} catch (RemoteException remoteException) {
			throw SysExceptionFactory.getException(remoteException);
		} catch (ServiceLocatorException serviceLocatorException) {
			throw SysExceptionFactory.getException(serviceLocatorException);
		}
		return responseDTO;
	}

}