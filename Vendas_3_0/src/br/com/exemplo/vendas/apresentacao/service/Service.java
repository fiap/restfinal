package br.com.exemplo.vendas.apresentacao.service;

import br.com.exemplo.vendas.apresentacao.delegate.BusinessDelegate;
import br.com.exemplo.vendas.negocio.model.vo.ClienteVO;
import br.com.exemplo.vendas.negocio.model.vo.CompraVO;
import br.com.exemplo.vendas.negocio.model.vo.PedidoVO;
import br.com.exemplo.vendas.negocio.model.vo.ProdutoVO;
import br.com.exemplo.vendas.negocio.model.vo.UsuarioVO;
import br.com.exemplo.vendas.util.dto.ServiceDTO;
import br.com.exemplo.vendas.util.exception.LayerException;

public class Service {
	public Boolean inserirUsuario(UsuarioVO vo) throws LayerException {
		ServiceDTO requestDTO = new ServiceDTO();
		ServiceDTO responseDTO = new ServiceDTO();

		requestDTO.set("usuarioVO", vo);
		responseDTO = BusinessDelegate.getInstance().inserirUsuario(requestDTO);
		Boolean sucesso = (Boolean) responseDTO.get("resposta");

		return sucesso;
	}

	public Boolean alterarUsuario(UsuarioVO vo) throws LayerException {
		ServiceDTO requestDTO = new ServiceDTO();
		ServiceDTO responseDTO = new ServiceDTO();

		requestDTO.set("usuarioVO", vo);
		responseDTO = BusinessDelegate.getInstance().alterarUsuario(requestDTO);
		Boolean sucesso = (Boolean) responseDTO.get("resposta");

		return sucesso;
	}

	public Boolean excluirUsuario(UsuarioVO vo) throws LayerException {
		ServiceDTO requestDTO = new ServiceDTO();
		ServiceDTO responseDTO = new ServiceDTO();

		requestDTO.set("usuarioVO", vo);
		responseDTO = BusinessDelegate.getInstance().excluirUsuario(requestDTO);
		Boolean sucesso = (Boolean) responseDTO.get("resposta");

		return sucesso;
	}

	public String enviarCompra(CompraVO vo) throws LayerException {
		String ticket = null;
		ServiceDTO requestDTO = new ServiceDTO();
		ServiceDTO responseDTO = new ServiceDTO();

		requestDTO.set("compraVO", vo);
		responseDTO = BusinessDelegate.getInstance().inserirQueue(requestDTO);
		Boolean sucesso = (Boolean) responseDTO.get("resposta");
		if (sucesso.booleanValue()) {
			ticket = (String) responseDTO.get("ticket");
		}
		return ticket;
	}
	
	public ClienteVO[] getClientesQuePossuemCompras() throws LayerException {
		ServiceDTO responseDTO = new ServiceDTO();

		responseDTO = BusinessDelegate.getInstance().getClientesQuePossuemCompras();
		ClienteVO[] retorno = (ClienteVO[]) responseDTO.get("resposta");

		return retorno;		
	}
	
	public CompraVO[] getComprasMenoresQueQuinhentos() throws LayerException {
		ServiceDTO responseDTO = new ServiceDTO();

		responseDTO = BusinessDelegate.getInstance().getComprasMenoresQueQuinhentos();
		CompraVO[] retorno = (CompraVO[]) responseDTO.get("resposta");

		return retorno;			
	}
	
	public PedidoVO[] getPedidosComReserva() throws LayerException {
		ServiceDTO responseDTO = new ServiceDTO();

		responseDTO = BusinessDelegate.getInstance().getPedidosComReserva();
		PedidoVO[] retorno = (PedidoVO[]) responseDTO.get("resposta");

		return retorno;			
	}
	
	public ProdutoVO[] getProdutosQueCustamMenosQueMil() throws LayerException {
		ServiceDTO responseDTO = new ServiceDTO();

		responseDTO = BusinessDelegate.getInstance().getProdutosQueCustamMenosQueMil();
		ProdutoVO[] retorno = (ProdutoVO[]) responseDTO.get("resposta");

		return retorno;
		
	}
}