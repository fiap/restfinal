package br.com.exemplo.vendas.apresentacao.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.exemplo.vendas.apresentacao.service.Service;
import br.com.exemplo.vendas.apresentacao.web.Action;
import br.com.exemplo.vendas.negocio.model.vo.CompraVO;
import br.com.exemplo.vendas.util.exception.LayerException;

public class ComprasMenoresQueQuinhentosACT implements Action {

	public String execute( HttpServletRequest request, HttpServletResponse response ) throws LayerException
	{
		Service service = new Service( ) ;
		CompraVO[] sucesso = (CompraVO[])service.getComprasMenoresQueQuinhentos();
		
		if ( sucesso != null && sucesso.length > 0 )
		{
			request.setAttribute( "sucesso", sucesso );
		}
		return null;
	}

}
