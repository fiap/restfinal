package br.com.exemplo.vendas.apresentacao.actions;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.exemplo.vendas.apresentacao.service.Service;
import br.com.exemplo.vendas.apresentacao.web.Action;
import br.com.exemplo.vendas.negocio.model.vo.ClienteVO;
import br.com.exemplo.vendas.util.exception.LayerException;

public class ClientesQuePossuemComprasACT implements Action {

	public String execute(HttpServletRequest request,
			HttpServletResponse response) throws LayerException {
		Service service = new Service();
		ClienteVO[] sucesso =
				(ClienteVO[])service.getClientesQuePossuemCompras();

//		ClienteVO[] sucesso = new ClienteVO[2];
//		ClienteVO clienteVO1 = new ClienteVO();
//		clienteVO1.setId(1).setNome("teste");

//		ClienteVO clienteVO2 = new ClienteVO();
//		clienteVO2.setId(2).setNome("teste2");

//		sucesso[0] = clienteVO1;
//		sucesso[1] = clienteVO2;

		List<ClienteVO> lista = Arrays.asList(sucesso);
		
		if (sucesso != null && sucesso.length > 0) {
			request.getSession().setAttribute("teste", "bat");
			request.getSession().setAttribute("sucesso", lista);
		}
		return "clientesQuePossuemCompras.jsp";
	}

}
