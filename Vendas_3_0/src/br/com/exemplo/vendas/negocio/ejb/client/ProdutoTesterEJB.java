package br.com.exemplo.vendas.negocio.ejb.client;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Hashtable;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;

import br.com.exemplo.vendas.negocio.interfaces.IProduto;
import br.com.exemplo.vendas.negocio.model.vo.ProdutoVO;
import br.com.exemplo.vendas.util.dto.ServiceDTO;

public class ProdutoTesterEJB {

	public static void main(String[] args) throws Exception
	{
		Hashtable prop = new Hashtable( );
		prop.put(InitialContext.INITIAL_CONTEXT_FACTORY, "org.jnp.interfaces.NamingContextFactory");
		prop.put(InitialContext.PROVIDER_URL, "jnp://localhost:1099");

		Context ctx = new InitialContext( prop ) ;
	   
		IProduto remote = (IProduto) ctx.lookup("ProdutoBean/remote");

		ServiceDTO requestDTO 	= new ServiceDTO( ) ;
		ServiceDTO responseDTO 	= new ServiceDTO( ) ;
		
		requestDTO.set("valor", new BigDecimal(110));
		requestDTO.set("quantidade", new BigInteger("2"));

		responseDTO = remote.consulta(requestDTO);
		ProdutoVO[] lista = (ProdutoVO[]) responseDTO.get("listaProdutos");
		
		if ( lista != null )
		{
			for ( int i = 0; i < lista.length; i++ )
            {
				ProdutoVO produtoVO = ( ProdutoVO ) lista[i] ;
	            System.out.println( produtoVO ) ;
            }
		}
	}

}
