package br.com.exemplo.vendas.negocio.ejb.client;

import java.math.BigDecimal;
import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.InitialContext;

import br.com.exemplo.vendas.negocio.interfaces.ICompra;
import br.com.exemplo.vendas.negocio.model.vo.CompraVO;
import br.com.exemplo.vendas.util.dto.ServiceDTO;

public class CompraTesterEJB {

	public static void main(String[] args) throws Exception
	{
		Hashtable prop = new Hashtable( );
		prop.put(InitialContext.INITIAL_CONTEXT_FACTORY, "org.jnp.interfaces.NamingContextFactory");
		prop.put(InitialContext.PROVIDER_URL, "jnp://localhost:1099");

		Context ctx = new InitialContext( prop ) ;
		
		ICompra remote = (ICompra) ctx.lookup("CompraBean/remote");
		
		ServiceDTO requestDTO = new ServiceDTO();
		requestDTO.set ("valorInicial", new BigDecimal(100));
		requestDTO.set("valorFinal", new BigDecimal(2000));
		
		ServiceDTO responseDTO = remote.consulta(requestDTO);
		CompraVO[] compras = (CompraVO[])responseDTO.get("listaCompras");
		
		if ( compras != null )
		{
			for ( int i = 0; i < compras.length; i++ )
            {
				CompraVO clienteVO = ( CompraVO ) compras[ i ] ;
	            System.out.println( clienteVO ) ;
            }
		}	
		
	}

}
