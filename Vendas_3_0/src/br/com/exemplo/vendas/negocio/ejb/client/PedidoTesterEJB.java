package br.com.exemplo.vendas.negocio.ejb.client;

import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.InitialContext;

import br.com.exemplo.vendas.negocio.interfaces.IPedido;
import br.com.exemplo.vendas.negocio.model.vo.CompraVO;
import br.com.exemplo.vendas.util.dto.ServiceDTO;

public class PedidoTesterEJB {

	public static void main(String[] args) throws Exception
	{
		Hashtable prop = new Hashtable( );
		prop.put(InitialContext.INITIAL_CONTEXT_FACTORY, "org.jnp.interfaces.NamingContextFactory");
		prop.put(InitialContext.PROVIDER_URL, "jnp://localhost:1099");

		Context ctx = new InitialContext( prop ) ;
		
		IPedido remote = (IPedido) ctx.lookup("PedidoBean/remote");
	
		ServiceDTO responseDTO = remote.consulta(null);
		CompraVO[] compras = (CompraVO[])responseDTO.get("listaPedidos");
		
		if ( compras != null )
		{
			for ( int i = 0; i < compras.length; i++ )
            {
				CompraVO clienteVO = ( CompraVO ) compras[ i ] ;
	            System.out.println( clienteVO ) ;
            }
		}	
	
	}

}
