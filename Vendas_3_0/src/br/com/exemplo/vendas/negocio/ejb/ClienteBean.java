package br.com.exemplo.vendas.negocio.ejb;

import java.rmi.RemoteException;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import br.com.exemplo.vendas.negocio.dao.DaoFactory;
import br.com.exemplo.vendas.negocio.ejb.interfaces.ClienteLocal;
import br.com.exemplo.vendas.negocio.ejb.interfaces.ClienteRemote;
import br.com.exemplo.vendas.negocio.entity.Cliente;
import br.com.exemplo.vendas.negocio.model.vo.ClienteVO;
import br.com.exemplo.vendas.util.dto.ServiceDTO;
import br.com.exemplo.vendas.util.exception.LayerException;

@Stateless
public class ClienteBean implements ClienteLocal, ClienteRemote {

	@PersistenceContext(unitName = "Vendas")
	EntityManager em;

	@Override
	public ServiceDTO consulta(ServiceDTO requestDTO) throws LayerException,
			RemoteException {
		ServiceDTO responseDTO = new ServiceDTO();
		
		List<Cliente> lista = DaoFactory.getClienteDao(em)
				.buscaComNumeroDeComprasMaiorQue(null);
		if ((lista != null) && (!lista.isEmpty())) {
			ClienteVO[] clientes = new ClienteVO[lista.size()];
			for (int i = 0; i < lista.size(); i++) {
				
				Cliente cliente = (Cliente) lista.get(i);
				
				ClienteVO clienteVO = new ClienteVO(cliente.getId(),
						cliente.getNome(), cliente.getEnderecoResidencial(),
						cliente.getEnderecoEntrega(), cliente.getTelefone(),
						cliente.getSituacao(), cliente.getCpf(),
						cliente.getRg(), cliente.getCnpj(), cliente.getIe());

				clientes[i] = clienteVO;
			}
			responseDTO.set("resposta", clientes);
		}
		return responseDTO;
	}
}
