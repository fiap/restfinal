package br.com.exemplo.vendas.negocio.ejb.interfaces;

import javax.ejb.Local;

import br.com.exemplo.vendas.negocio.interfaces.IProduto;

@Local
public interface ProdutoLocal extends IProduto {
	
}
