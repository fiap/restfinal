package br.com.exemplo.vendas.negocio.ejb.client;

import java.math.BigDecimal;
import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.InitialContext;

import br.com.exemplo.vendas.negocio.interfaces.ICompra;
import br.com.exemplo.vendas.negocio.model.vo.CompraVO;
import br.com.exemplo.vendas.util.dto.ServiceDTO;

public class TesterEJB {

	public static void main(String[] args) throws Exception
	{
		Hashtable prop = new Hashtable( );
		prop.put(InitialContext.INITIAL_CONTEXT_FACTORY, "org.jnp.interfaces.NamingContextFactory");
		prop.put(InitialContext.PROVIDER_URL, "jnp://localhost:1099");

		Context ctx = new InitialContext( prop ) ;
		
//		ICliente remote = (ICliente) ctx.lookup("ClienteBean/remote");
		ICompra remote = (ICompra) ctx.lookup("CompraBean/remote");
		
		ServiceDTO requestDTO = new ServiceDTO();
		requestDTO.set ("valorInicial", new BigDecimal(100));
		requestDTO.set("valorFinal", new BigDecimal(500));
		
		
		ServiceDTO responseDTO = remote.consulta(requestDTO);
		//ClienteVO[] clientes = (ClienteVO[])responseDTO.get("listaClientes");
		CompraVO[] compras = (CompraVO[])responseDTO.get("listaCompras");
		
//		if ( clientes != null )
//		{
//			for ( int i = 0; i < clientes.length; i++ )
//            {
//	            ClienteVO clienteVO = ( ClienteVO ) clientes[ i ] ;
//	            System.out.println( clienteVO ) ;
//            }
//		}
	
		if ( compras != null )
		{
			for ( int i = 0; i < compras.length; i++ )
            {
				CompraVO clienteVO = ( CompraVO ) compras[ i ] ;
	            System.out.println( clienteVO ) ;
            }
		}	
		
//		UsuarioInterface remote = (UsuarioInterface) ctx.lookup("UsuarioBean/remote");
		
//		ServiceDTO requestDTO 	= new ServiceDTO( ) ;
//		ServiceDTO responseDTO 	= new ServiceDTO( ) ;
//
//		/**
//		 * Inserir usuario
//		 */
//		UsuarioVO vo = new UsuarioVO( "marcao1", "senha1111", "grupo1111", "perfil1111", "S", new Date( ), "marcos macedo" ) ;
//		requestDTO.set("usuarioVO", vo ) ;
//		responseDTO = remote.inserirUsuario( requestDTO ) ;
//		Boolean sucesso = ( Boolean ) responseDTO.get("resposta") ;
//		if ( sucesso.booleanValue( ) )
//		{
//			System.out.println("Sucesso na execu��o do processo!");
//		}

//		/**
//		 * Alterar usuario
//		 */
//		UsuarioVO vo = new UsuarioVO( "marcao", "123456", "grupo3333", "perfi4444", "S", new Date( ), "marcos r. macedo" ) ;
//		requestDTO.set("usuarioVO", vo ) ;
//		responseDTO = remote.alterarUsuario( requestDTO ) ;
//		Boolean sucesso = ( Boolean ) responseDTO.get("resposta") ;
//		if ( sucesso.booleanValue( ) )
//		{
//			System.out.println("Sucesso na execu��o do processo!");
//		}

//		/**
//		 * Excluir usuario
//		 */
//		UsuarioVO vo = new UsuarioVO( "marcao", "123456", "grupo3333", "perfi4444", "S", new Date( ), "marcos r. macedo" ) ;
//		requestDTO.set("usuarioVO", vo ) ;
//		responseDTO = remote.excluirUsuario(  requestDTO ) ;
//		Boolean sucesso = ( Boolean ) responseDTO.get("resposta") ;
//		if ( sucesso.booleanValue( ) )
//		{
//			System.out.println("Sucesso na execu��o do processo!");
//		}
		
		/**
		 * Consultar usuario
		 */
//		UsuarioVO vo = new UsuarioVO( "marcao", "123456", "grupo3333", "perfi4444", "S", new Date( ), "marcos r. macedo" ) ;
//		requestDTO.set("usuarioVO", vo ) ;
//		responseDTO = remote.selecionarTodosUsuario( requestDTO ) ;
//		UsuarioVO[ ] lista = ( UsuarioVO[ ] ) responseDTO.get( "listaUsuario" ) ;
//		if ( lista != null )
//		{
//			for ( int i = 0; i < lista.length; i++ )
//            {
//	            UsuarioVO usuarioVO = ( UsuarioVO ) lista[ i ] ;
//	            System.out.println( usuarioVO ) ;
//            }
//		}
	}

}
