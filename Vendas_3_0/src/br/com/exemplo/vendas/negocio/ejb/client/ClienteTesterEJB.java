package br.com.exemplo.vendas.negocio.ejb.client;

import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.InitialContext;

import br.com.exemplo.vendas.negocio.interfaces.ICliente;
import br.com.exemplo.vendas.negocio.model.vo.ClienteVO;
import br.com.exemplo.vendas.util.dto.ServiceDTO;

public class ClienteTesterEJB {

	public static void main(String[] args) throws Exception
	{
		Hashtable prop = new Hashtable( );
		prop.put(InitialContext.INITIAL_CONTEXT_FACTORY, "org.jnp.interfaces.NamingContextFactory");
		prop.put(InitialContext.PROVIDER_URL, "jnp://localhost:1099");

		Context ctx = new InitialContext( prop ) ;
		
		ICliente remote = (ICliente) ctx.lookup("ClienteBean/remote");
	
		ServiceDTO responseDTO = remote.consulta(null);
		ClienteVO[] clientes = (ClienteVO[])responseDTO.get("listaClientes");
		
		if ( clientes != null )
		{
			for ( int i = 0; i < clientes.length; i++ )
            {
	            ClienteVO clienteVO = ( ClienteVO ) clientes[ i ] ;
	            System.out.println("Id: " +  clienteVO.getId()) ;
	            System.out.println("Nome: " +  clienteVO.getNome()) ;
            }
		}
	
	}

}
