package br.com.exemplo.vendas.negocio.ejb.interfaces;

import javax.ejb.Local;

import br.com.exemplo.vendas.negocio.interfaces.ICliente;

@Local
public interface ClienteLocal extends ICliente {
	
}
