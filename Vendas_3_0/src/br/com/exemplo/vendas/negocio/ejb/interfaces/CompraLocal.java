package br.com.exemplo.vendas.negocio.ejb.interfaces;

import javax.ejb.Local;

import br.com.exemplo.vendas.negocio.interfaces.ICompra;

@Local
public interface CompraLocal extends ICompra{
	
}
