package br.com.exemplo.vendas.negocio.ejb;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import br.com.exemplo.vendas.negocio.dao.DaoFactory;
import br.com.exemplo.vendas.negocio.ejb.interfaces.CompraLocal;
import br.com.exemplo.vendas.negocio.ejb.interfaces.CompraRemote;
import br.com.exemplo.vendas.negocio.entity.Compra;
import br.com.exemplo.vendas.negocio.model.vo.CompraVO;
import br.com.exemplo.vendas.util.dto.ServiceDTO;
import br.com.exemplo.vendas.util.exception.LayerException;

@Stateless
public class CompraBean implements CompraLocal, CompraRemote {

	@PersistenceContext(unitName = "Vendas")
	EntityManager em;

	@Override
	public ServiceDTO consulta(ServiceDTO requestDTO) throws LayerException,
			RemoteException {

		ServiceDTO responseDTO = new ServiceDTO();

		BigDecimal valorInicial = (BigDecimal) requestDTO.get("valorInicial");
		BigDecimal valorFinal = (BigDecimal) requestDTO.get("valorFinal");

		List<Compra> lista = DaoFactory.getCompraDAO(em)
				.buscaCompraComValorEntre(valorInicial, valorFinal);

		if ((lista != null) && (!lista.isEmpty())) {
			CompraVO[] compras = new CompraVO[lista.size()];
			for (int i = 0; i < lista.size(); i++) {

				Compra compra = (Compra) lista.get(i);

				CompraVO compraVO = new CompraVO(compra.getId(),
						compra.getData(), compra.getResponsavel(),
						compra.getSituacao(), compra.getValor(), compra
								.getCliente().getId());
				
				if (compra.getReserva() != null) {
					compraVO.setReserva(compra.getReserva().getId());
				}

				compras[i] = compraVO;
			}
			responseDTO.set("resposta", compras);
		}
		return responseDTO;
	}

}
