package br.com.exemplo.vendas.negocio.ejb;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.rmi.RemoteException;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import br.com.exemplo.vendas.negocio.dao.DaoFactory;
import br.com.exemplo.vendas.negocio.ejb.interfaces.ProdutoLocal;
import br.com.exemplo.vendas.negocio.ejb.interfaces.ProdutoRemote;
import br.com.exemplo.vendas.negocio.entity.Produto;
import br.com.exemplo.vendas.negocio.model.vo.ProdutoVO;
import br.com.exemplo.vendas.util.dto.ServiceDTO;
import br.com.exemplo.vendas.util.exception.LayerException;

@Stateless
public class ProdutoBean implements ProdutoLocal, ProdutoRemote {

	@PersistenceContext(unitName = "Vendas")
	EntityManager em;

	@Override
	public ServiceDTO consulta(ServiceDTO requestDTO) throws LayerException,
			RemoteException {

		ServiceDTO responseDTO = new ServiceDTO();

		BigDecimal valor = (BigDecimal) requestDTO.get("valor");
		BigInteger quantidade = (BigInteger) requestDTO.get("quantidade");

		List<Produto> lista = DaoFactory.getProdutoDACompraDAO(em)
				.buscaComValorEQtdMinimaNoEstoque(valor, quantidade);
		if ((lista != null) && (!lista.isEmpty())) {
			ProdutoVO[] produtos = new ProdutoVO[lista.size()];
			for (int i = 0; i < lista.size(); i++) {

				Produto produto = (Produto) lista.get(i);

				ProdutoVO produtoVO = new ProdutoVO(produto.getId(),
						produto.getDescricao(), produto.getPreco(), 
						produto.getEstoque());

				produtos[i] = produtoVO;
			}
			responseDTO.set("resposta", produtos);
		}
		return responseDTO;

	}

}
