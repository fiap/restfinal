package br.com.exemplo.vendas.negocio.interfaces;

public interface IPedido {
	public br.com.exemplo.vendas.util.dto.ServiceDTO consulta( br.com.exemplo.vendas.util.dto.ServiceDTO requestDTO )
	        throws br.com.exemplo.vendas.util.exception.LayerException,
	        java.rmi.RemoteException;
}
