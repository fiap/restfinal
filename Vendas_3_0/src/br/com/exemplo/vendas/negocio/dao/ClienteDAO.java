package br.com.exemplo.vendas.negocio.dao;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.exemplo.vendas.negocio.entity.Cliente;
import br.com.exemplo.vendas.negocio.entity.Compra;

public class ClienteDAO extends GenericDAO<Cliente> {

	public ClienteDAO(EntityManager em) {
		super(em);
	}

	public List<Cliente> buscaComNumeroDeComprasMaiorQue(final BigInteger limite){
		List<Cliente> clientes = new ArrayList<Cliente>();

		try
		{
			Query query = em.createQuery( "SELECT DISTINCT(c.cliente) from Compra c" );
			clientes = query.getResultList();
		}
		catch ( Exception e )
		{
			if ( debugInfo )
			{
				e.printStackTrace( );
			}
		}
		return clientes;
	}
}
