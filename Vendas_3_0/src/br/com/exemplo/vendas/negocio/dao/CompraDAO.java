package br.com.exemplo.vendas.negocio.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.exemplo.vendas.negocio.entity.Compra;

public class CompraDAO extends GenericDAO<Compra> {

	public CompraDAO(EntityManager em) {
		super(em);
	}
	
	public List<Compra> buscaCompraComValorEntre(BigDecimal valorInicial, BigDecimal valorFinal) {
		List<Compra> compras = new ArrayList<Compra>();

		try
		{
			
			Query query = em.createQuery("SELECT c FROM Compra c "
					+ " where c.valor between :valorInicial AND :valorFinal" );
			query.setParameter( "valorInicial", valorInicial );
			query.setParameter( "valorFinal", valorFinal );
			compras = query.getResultList();
		}
		catch ( Exception e )
		{
			e.printStackTrace();
			if ( debugInfo )
			{
				e.printStackTrace( );
			}
		}
		return compras;
	}

	public List<Compra> buscaPedidoComReserva(){
		List<Compra> compras = new ArrayList<Compra>();

		try
		{
			Query query = em.createQuery( "SELECT c from Compra c WHERE c.reserva is not null" );
			compras = query.getResultList();
		}
		catch ( Exception e )
		{
			if ( debugInfo )
			{
				e.printStackTrace( );
			}
		}
		return compras;
	}
	
}
