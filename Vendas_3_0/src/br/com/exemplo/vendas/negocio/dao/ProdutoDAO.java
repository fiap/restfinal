package br.com.exemplo.vendas.negocio.dao;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.exemplo.vendas.negocio.entity.Compra;
import br.com.exemplo.vendas.negocio.entity.Produto;

public class ProdutoDAO extends GenericDAO<Produto> {

	public ProdutoDAO(EntityManager em) {
		super(em);
	}

	public List<Produto> buscaComValorEQtdMinimaNoEstoque(BigDecimal valor, BigInteger qtd){
		List<Produto> produtos = new ArrayList<Produto>();

		try
		{
			Query query = em.createQuery( "from Produto where preco < :preco AND CAST(estoque AS Integer) >= :qtdEstoque" );
			query.setParameter( "preco", valor );
			query.setParameter( "qtdEstoque", qtd );
			produtos = query.getResultList();
		}
		catch ( Exception e )
		{
			if ( debugInfo )
			{
				e.printStackTrace( );
			}
		}
		
		return produtos;
	}
}
