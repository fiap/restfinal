package br.com.exemplo.vendas.negocio.model.vo;

import java.io.Serializable;

public class ClienteVO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer id;
	private String nome;
	private String enderecoResidencial;
	private String enderecoEntrega;
	private String telefone;
	private String situacao;
	private String cpf;
	private String rg;
	private String cnpj;
	private String ie;

	public String getNome() {
		return nome;
	}

	public ClienteVO() {}
	
	public ClienteVO(Integer id, String nome, String enderecoResidencial,
			String enderecoEntrega, String telefone, String situacao,
			String cpf, String rg, String cnpj, String ie) {
		super();
		this.id = id;
		this.nome = nome;
		this.enderecoResidencial = enderecoResidencial;
		this.enderecoEntrega = enderecoEntrega;
		this.telefone = telefone;
		this.situacao = situacao;
		this.cpf = cpf;
		this.rg = rg;
		this.cnpj = cnpj;
		this.ie = ie;
	}

	public ClienteVO setNome(String nome) {
		this.nome = nome;
		return this;
	}

	public String getTelefone() {
		return telefone;
	}

	public ClienteVO setTelefone(String telefone) {
		this.telefone = telefone;
		return this;
	}

	public String getSituacao() {
		return situacao;
	}

	public ClienteVO setSituacao(String situacao) {
		this.situacao = situacao;
		return this;
	}

	public Integer getId() {
		return id;
	}

	public ClienteVO setId(Integer id) {
		this.id = id;
		return this;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getIe() {
		return ie;
	}

	public void setIe(String ie) {
		this.ie = ie;
	}

	public String getEnderecoResidencial() {
		return enderecoResidencial;
	}

	public ClienteVO setEnderecoResidencial(String enderecoResidencial) {
		this.enderecoResidencial = enderecoResidencial;
		return this;
	}

	public String getEnderecoEntrega() {
		return enderecoEntrega;
	}

	public ClienteVO setEnderecoEntrega(String enderecoEntrega) {
		this.enderecoEntrega = enderecoEntrega;
		return this;
	}
}
