package br.com.exemplo.vendas.negocio.model.vo;

import java.io.Serializable;
import java.math.BigDecimal;

public class ProdutoVO implements Serializable {


	private static final long serialVersionUID = 1L;

	private Integer id;
	private String descricao;
	private BigDecimal preco;
	private String estoque;

	public ProdutoVO() {
		
	}
	
	public ProdutoVO(Integer id, String descricao,
			BigDecimal preco, String estoque) {
		super();
		this.id = id;
		this.descricao = descricao;
		this.preco = preco;
		this.estoque = estoque;
	}

	public Integer getId() {
		return id;
	}

	public ProdutoVO setId(Integer id) {
		this.id = id;
		return this;
	}

	public String getDescricao() {
		return descricao;
	}

	public ProdutoVO setDescricao(String descricao) {
		this.descricao = descricao;
		return this;
	}

	public BigDecimal getPreco() {
		return preco;
	}

	public ProdutoVO setPreco(BigDecimal preco) {
		this.preco = preco;
		return this;
	}

	public String getEstoque() {
		return estoque;
	}

	public ProdutoVO setEstoque(String estoque) {
		this.estoque = estoque;
		return this;
	}
}
