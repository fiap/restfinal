package br.com.exemplo.vendas.negocio.model.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class ReservaVO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer id;
	private Date data;
	private String atendente;
	private String situacao;
	private BigDecimal valor;
	private ClienteVO cliente;

	public Integer getId() {
		return id;
	}

	public ReservaVO setId(Integer id) {
		this.id = id;
		return this;
	}

	public Date getData() {
		return data;
	}

	public ReservaVO setData(Date data) {
		this.data = data;
		return this;
	}

	public String getAtendente() {
		return atendente;
	}

	public ReservaVO setAtendente(String atendente) {
		this.atendente = atendente;
		return this;
	}

	public String getSituacao() {
		return situacao;
	}

	public ReservaVO setSituacao(String situacao) {
		this.situacao = situacao;
		return this;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public ReservaVO setValor(BigDecimal valor) {
		this.valor = valor;
		return this;
	}

	public ClienteVO getCliente() {
		return cliente;
	}

	public ReservaVO setCliente(ClienteVO cliente) {
		this.cliente = cliente;
		return this;
	}
}
