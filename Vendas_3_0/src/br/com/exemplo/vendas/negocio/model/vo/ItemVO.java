package br.com.exemplo.vendas.negocio.model.vo;

import java.io.Serializable;
import java.math.BigInteger;

public class ItemVO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer id;
	private BigInteger quantidade;
	private String situacao;
	private ReservaVO reserva;
	private CompraVO compra;
	private ProdutoVO produto;

	public Integer getId() {
		return id;
	}

	public ItemVO setId(Integer id) {
		this.id = id;
		return this;
	}

	public BigInteger getQuantidade() {
		return quantidade;
	}

	public ItemVO setQuantidade(BigInteger quantidade) {
		this.quantidade = quantidade;
		return this;
	}

	public String getSituacao() {
		return situacao;
	}

	public ItemVO setSituacao(String situacao) {
		this.situacao = situacao;
		return this;
	}

	public ReservaVO getReserva() {
		return reserva;
	}

	public ItemVO setReserva(ReservaVO reserva) {
		this.reserva = reserva;
		return this;
	}

	public CompraVO getCompra() {
		return compra;
	}

	public ItemVO setCompra(CompraVO compra) {
		this.compra = compra;
		return this;
	}

	public ProdutoVO getProduto() {
		return produto;
	}

	public ItemVO setProduto(ProdutoVO produto) {
		this.produto = produto;
		return this;
	}

}
