package br.com.exemplo.vendas.negocio.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Reserva implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	private Integer id;
	
	private Date data;
	private String atendente;
	private String situacao;
	private BigDecimal valor;
	
	@ManyToOne
	@JoinColumn(name="cliente_id")
	private Cliente cliente;

	public Integer getId() {
		return id;
	}

	public Reserva setId(Integer id) {
		this.id = id;
		return this;
	}

	public Date getData() {
		return data;
	}

	public Reserva setData(Date data) {
		this.data = data;
		return this;
	}

	public String getAtendente() {
		return atendente;
	}

	public Reserva setAtendente(String atendente) {
		this.atendente = atendente;
		return this;
	}

	public String getSituacao() {
		return situacao;
	}

	public Reserva setSituacao(String situacao) {
		this.situacao = situacao;
		return this;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public Reserva setValor(BigDecimal valor) {
		this.valor = valor;
		return this;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public Reserva setCliente(Cliente cliente) {
		this.cliente = cliente;
		return this;
	}
}
