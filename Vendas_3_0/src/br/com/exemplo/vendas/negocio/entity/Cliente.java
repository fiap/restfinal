package br.com.exemplo.vendas.negocio.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Cliente implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	private Integer id;
	
	private String nome;
	private String enderecoResidencial;
	private String enderecoEntrega;
	private String telefone;
	private String situacao;
	private String cnpj;
	private String ie;
	private String cpf;
	private String rg;

	public String getNome() {
		return nome;
	}

	public Cliente setNome(String nome) {
		this.nome = nome;
		return this;
	}

	public String getTelefone() {
		return telefone;
	}

	public Cliente setTelefone(String telefone) {
		this.telefone = telefone;
		return this;
	}

	public String getSituacao() {
		return situacao;
	}

	public Cliente setSituacao(String situacao) {
		this.situacao = situacao;
		return this;
	}

	public Integer getId() {
		return id;
	}

	public Cliente setId(Integer id) {
		this.id = id;
		return this;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getIe() {
		return ie;
	}

	public void setIe(String ie) {
		this.ie = ie;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public String getEnderecoResidencial() {
		return enderecoResidencial;
	}

	public Cliente setEnderecoResidencial(String enderecoResidencial) {
		this.enderecoResidencial = enderecoResidencial;
		return this;
	}

	public String getEnderecoEntrega() {
		return enderecoEntrega;
	}

	public Cliente setEnderecoEntrega(String enderecoEntrega) {
		this.enderecoEntrega = enderecoEntrega;
		return this;
	}
}
