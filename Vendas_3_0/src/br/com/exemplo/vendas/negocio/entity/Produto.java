package br.com.exemplo.vendas.negocio.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Produto implements Serializable {


	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	private Integer id;
	private String descricao;
	private BigDecimal preco;
	private String estoque;

	public Integer getId() {
		return id;
	}

	public Produto setId(Integer id) {
		this.id = id;
		return this;
	}

	public String getDescricao() {
		return descricao;
	}

	public Produto setDescricao(String descricao) {
		this.descricao = descricao;
		return this;
	}

	public BigDecimal getPreco() {
		return preco;
	}

	public Produto setPreco(BigDecimal preco) {
		this.preco = preco;
		return this;
	}

	public String getEstoque() {
		return estoque;
	}

	public Produto setEstoque(String estoque) {
		this.estoque = estoque;
		return this;
	}
}
