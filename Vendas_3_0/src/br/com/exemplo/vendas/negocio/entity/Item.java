package br.com.exemplo.vendas.negocio.entity;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Item implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	private Integer id;
	
	private BigInteger quantidade;
	private String situacao;
	private Reserva reserva;
	private Compra compra;
	private Produto produto;

	public Integer getId() {
		return id;
	}

	public Item setId(Integer id) {
		this.id = id;
		return this;
	}

	public BigInteger getQuantidade() {
		return quantidade;
	}

	public Item setQuantidade(BigInteger quantidade) {
		this.quantidade = quantidade;
		return this;
	}

	public String getSituacao() {
		return situacao;
	}

	public Item setSituacao(String situacao) {
		this.situacao = situacao;
		return this;
	}

	public Reserva getReserva() {
		return reserva;
	}

	public Item setReserva(Reserva reserva) {
		this.reserva = reserva;
		return this;
	}

	public Compra getCompra() {
		return compra;
	}

	public Item setCompra(Compra compra) {
		this.compra = compra;
		return this;
	}

	public Produto getProduto() {
		return produto;
	}

	public Item setProduto(Produto produto) {
		this.produto = produto;
		return this;
	}

}
