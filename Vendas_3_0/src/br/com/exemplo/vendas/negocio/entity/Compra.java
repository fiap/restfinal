package br.com.exemplo.vendas.negocio.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="compra")
public class Compra implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	private Integer id;
	
	private Date data;
	private String responsavel;
	private String situacao;
	private BigDecimal valor;
	
	@ManyToOne
	@JoinColumn(name="reserva_id")
	private Reserva reserva;
	
	@ManyToOne
	@JoinColumn(name="cliente_id")
	private Cliente cliente;

	public Date getData() {
		return data;
	}

	public Compra setData(Date data) {
		this.data = data;
		return this;
	}

	public String getResponsavel() {
		return responsavel;
	}

	public Compra setResponsavel(String responsavel) {
		this.responsavel = responsavel;
		return this;
	}

	public String getSituacao() {
		return situacao;
	}

	public Compra setSituacao(String situacao) {
		this.situacao = situacao;
		return this;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public Compra setValor(BigDecimal valor) {
		this.valor = valor;
		return this;
	}

	public Reserva getReserva() {
		return reserva;
	}

	public void setReserva(Reserva reserva) {
		this.reserva = reserva;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Integer getId() {
		return id;
	}

	public Compra setId(Integer id) {
		this.id = id;
		return this;
	}

}
