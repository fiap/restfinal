package br.com.exemplo.vendas.negocio.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Usuario {

	@Id
	private String login;
	private String senha;
	private String grupo;
	private String perfil;
	private String bloqueado;
	private Date ultimoAcesso;
	private String nome;

	public Usuario() {
	}

	public Usuario(String login, String senha, String grupo, String perfil,
			String bloqueado, Date ultimoAcesso, String nome) {
		this.login = login;
		this.senha = senha;
		this.grupo = grupo;
		this.perfil = perfil;
		this.bloqueado = bloqueado;
		this.ultimoAcesso = ultimoAcesso;
		this.nome = nome;
	}

	public String getNome() {
		return nome;
	}

	public Usuario setNome(String nome) {
		this.nome = nome;
		return this;
	}

	public String getLogin() {
		return login;
	}

	public Usuario setLogin(String login) {
		this.login = login;
		return this;
	}

	public String getSenha() {
		return senha;
	}

	public Usuario setSenha(String senha) {
		this.senha = senha;
		return this;
	}

	public String getGrupo() {
		return grupo;
	}

	public Usuario setGrupo(String grupo) {
		this.grupo = grupo;
		return this;
	}

	public String getPerfil() {
		return perfil;
	}

	public Usuario setPerfil(String perfil) {
		this.perfil = perfil;
		return this;
	}

	public String getBloqueado() {
		return bloqueado;
	}

	public Usuario setBloqueado(String bloqueado) {
		this.bloqueado = bloqueado;
		return this;
	}

	public Date getUltimoAcesso() {
		return ultimoAcesso;
	}

	public Usuario setUltimoAcesso(Date ultimoAcesso) {
		this.ultimoAcesso = ultimoAcesso;
		return this;
	}

}
