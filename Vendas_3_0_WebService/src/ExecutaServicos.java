import java.util.Date;

import br.com.exemplo.vendas.apresentacao.service.Service;
import br.com.exemplo.vendas.negocio.model.vo.ClienteVO;
import br.com.exemplo.vendas.negocio.model.vo.CompraVO;
import br.com.exemplo.vendas.negocio.model.vo.PedidoVO;
import br.com.exemplo.vendas.negocio.model.vo.ProdutoVO;
import br.com.exemplo.vendas.negocio.model.vo.UsuarioVO;
import br.com.exemplo.vendas.util.exception.BusinessException;
import br.com.exemplo.vendas.util.exception.LayerException;
import br.com.exemplo.vendas.util.exception.SysException;

public class ExecutaServicos {
	public Boolean inserirUsuario(UsuarioVO vo) throws Exception {
		Boolean retorno = new Boolean(false);
		try {
			Service service = new Service();
			retorno = service.inserirUsuario(vo);
		} catch (LayerException e) {
			if (e instanceof SysException) {
				throw new Exception("erro interno no sistema: "
						+ e.getMessage());
			} else {
				if (e instanceof BusinessException) {
					throw new Exception("erro de neg�cio: " + e.getMessage());
				}
			}
		}
		return retorno;
	}

	public String enviaMensagem(CompraVO vo) throws Exception {
		String retorno = null;

		vo.setCliente(10000);
		vo.setData(new Date());
		vo.setResponsavel("enviando um teste");

		try {
			Service service = new Service();
			retorno = service.enviarCompra(vo);
		} catch (LayerException e) {
			if (e instanceof SysException) {
				throw new Exception("erro interno no sistema: "
						+ e.getMessage());
			} else {
				if (e instanceof BusinessException) {
					throw new Exception("erro de neg�cio: " + e.getMessage());
				}
			}
		}
		return retorno;
	}
	
	/**
	 * Localiza todos os Clientes que possuam pelo menos uma Compra realizada
	 * 
	 * @return 
	 * @throws Exception 
	 */
	public ClienteVO[] getClientesQuePossuemCompras() throws Exception {
		
		ClienteVO[] retorno = null;
		try {
			Service service = new Service();
			retorno = service.getClientesQuePossuemCompras();
		} catch (LayerException e) {
			if (e instanceof SysException) {
				throw new Exception("erro interno no sistema: "
						+ e.getMessage());
			} else {
				if (e instanceof BusinessException) {
					throw new Exception("erro de neg�cio: " + e.getMessage());
				}
			}
		}
		return retorno;

	
	}

	/**
	 * Localiza todas as compras menores que 500
	 * 
	 * @return
	 * @throws Exception 
	 */
	public CompraVO[] getComprasMenoresQueQuinhentos() throws Exception {
		
		CompraVO[] retorno = null;
		try {
			Service service = new Service();
			retorno = service.getComprasMenoresQueQuinhentos();
		} catch (LayerException e) {
			if (e instanceof SysException) {
				throw new Exception("erro interno no sistema: "
						+ e.getMessage());
			} else {
				if (e instanceof BusinessException) {
					throw new Exception("erro de neg�cio: " + e.getMessage());
				}
			}
		}
		return retorno;
}
	
	/**
	 * Localiza todas as compras geradas atrav�s de uma reserva
	 * 
	 * @return
	 * @throws Exception 
	 */
	public PedidoVO[] getPedidosComReserva() throws Exception {
		
		PedidoVO[] retorno = null;
		try {
			Service service = new Service();
			retorno = service.getPedidosComReserva();
		} catch (LayerException e) {
			if (e instanceof SysException) {
				throw new Exception("erro interno no sistema: "
						+ e.getMessage());
			} else {
				if (e instanceof BusinessException) {
					throw new Exception("erro de neg�cio: " + e.getMessage());
				}
			}
		}
		return retorno;
	}

	public ProdutoVO[] getProdutosQueCustamMenosQueMil() throws Exception {
		
		ProdutoVO[] retorno = null;
		try {
			Service service = new Service();
			retorno = service.getProdutosQueCustamMenosQueMil();
		} catch (LayerException e) {
			if (e instanceof SysException) {
				throw new Exception("erro interno no sistema: "
						+ e.getMessage());
			} else {
				if (e instanceof BusinessException) {
					throw new Exception("erro de neg�cio: " + e.getMessage());
				}
			}
		}
		return retorno;
	}	
	
}
