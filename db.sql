SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci ;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `mydb`.`Cliente`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mydb`.`Cliente` ;

CREATE  TABLE IF NOT EXISTS `mydb`.`Cliente` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `nome` VARCHAR(45) NOT NULL ,
  `enderecoResidencial` VARCHAR(200) NOT NULL ,
  `enderecoEntrega` VARCHAR(200) NULL ,
  `telefone` VARCHAR(15) NULL ,
  `situacao` VARCHAR(1) NOT NULL ,
  `rg` VARCHAR(45) NULL ,
  `cpf` VARCHAR(45) NULL ,
  `ie` VARCHAR(45) NULL ,
  `cnpj` VARCHAR(45) NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;

CREATE UNIQUE INDEX `id_UNIQUE` ON `mydb`.`Cliente` (`id` ASC) ;


-- -----------------------------------------------------
-- Table `mydb`.`Reserva`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mydb`.`Reserva` ;

CREATE  TABLE IF NOT EXISTS `mydb`.`Reserva` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `data` DATETIME NOT NULL ,
  `atendente` VARCHAR(45) NULL ,
  `situacao` VARCHAR(1) NOT NULL ,
  `valor` DECIMAL(10,2) NOT NULL ,
  `cliente_id` INT NOT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;

CREATE UNIQUE INDEX `id_UNIQUE` ON `mydb`.`Reserva` (`id` ASC) ;


-- -----------------------------------------------------
-- Table `mydb`.`Compra`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mydb`.`Compra` ;

CREATE  TABLE IF NOT EXISTS `mydb`.`Compra` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `data` DATETIME NOT NULL ,
  `responsavel` VARCHAR(45) NULL ,
  `situacao` VARCHAR(1) NOT NULL ,
  `valor` DECIMAL(10,2) NOT NULL ,
  `reserva_id` INT NULL ,
  `cliente_id` INT NOT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;

CREATE UNIQUE INDEX `id_UNIQUE` ON `mydb`.`Compra` (`id` ASC) ;


-- -----------------------------------------------------
-- Table `mydb`.`Produto`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mydb`.`Produto` ;

CREATE  TABLE IF NOT EXISTS `mydb`.`Produto` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `descricao` VARCHAR(45) NOT NULL ,
  `preco` DECIMAL(10,2) NOT NULL ,
  `estoque` VARCHAR(10) NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;

CREATE UNIQUE INDEX `id_UNIQUE` ON `mydb`.`Produto` (`id` ASC) ;


-- -----------------------------------------------------
-- Table `mydb`.`Item`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mydb`.`Item` ;

CREATE  TABLE IF NOT EXISTS `mydb`.`Item` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `quantidade` DECIMAL(10,2) NOT NULL ,
  `situacao` VARCHAR(1) NOT NULL ,
  `reserva_id` INT NULL ,
  `Compra_id` INT NULL ,
  `produto_id` INT NOT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;

CREATE UNIQUE INDEX `id_UNIQUE` ON `mydb`.`Item` (`id` ASC) ;


-- -----------------------------------------------------
-- Table `mydb`.`Usuario`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mydb`.`Usuario` ;

CREATE  TABLE IF NOT EXISTS `mydb`.`Usuario` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `login` VARCHAR(45) NOT NULL ,
  `senha` VARCHAR(45) NOT NULL ,
  `grupo` VARCHAR(45) NULL ,
  `perfil` VARCHAR(45) NULL ,
  `bloqueado` INT NOT NULL DEFAULT 1 ,
  `Cliente_id` INT NOT NULL ,
  PRIMARY KEY (`id`, `Cliente_id`) )
ENGINE = InnoDB;

CREATE UNIQUE INDEX `id_UNIQUE` ON `mydb`.`Usuario` (`id` ASC) ;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
